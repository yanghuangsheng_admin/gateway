<?php
/**
 * Created by sublime.
 * User: jws
 * Date: 2019/12/23
 * Time: 16:38
 */

namespace app\common\model;
use think\Model;
use think\model\concern\SoftDelete;

class User extends Model{
	use SoftDelete;
	protected $deleteTime = 'delete_time';
    protected $defaultSoftDelete = 0;

    protected  $autoWriteTimestamp = true;

    /**
     * @param string 用户名
     * 查看用户
     * @return array
     */
    public function findUser($username) {
    	return $this->where('username', $username)->find();
    }

    /**
     * 添加用户
     * @param $data array
     */
    public function addUser($data) {
    	if(!is_array($data)){
            $this->except('参数错误');
        }
        $this->allowField(true)->save($data);
        return $this->id;
    }

    /**
     * 查看username
     * @param $id 描述
     */
    public function findUserById($id) {
        return $this->where('id', $id)->find();
    }

    /**
     * 除去该用户外
     * @param  $id        聊天发起者
     * @return [type]     [description]
     */
    public function getuser($id) {
        return $this->where([['status', 'eq', '1'],['id', 'neq', $id]])->select();
    }
}
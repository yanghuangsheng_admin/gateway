<?php
namespace app\common\lib;

class IAes {
	/**
     * Aes加密
     * @param $input
     * @param $key
     * @return string
     */
    public static function encryptAes($input, $sKey = '')
    {
        $sKey = $sKey ? $sKey : 'monkey_aes';
        $data = openssl_encrypt($input, 'AES-128-ECB', $sKey, OPENSSL_RAW_DATA);
        $data = base64_encode($data);
        return $data;
    }

    /**
     * Aes解密
     * @param $sStr
     * @param $sKey
     * @return string
     */
    public static function decryptAes($sStr, $sKey = '')
    {
        $sKey = $sKey ? $sKey : 'monkey_aes';
        $decrypted = openssl_decrypt(base64_decode($sStr), 'AES-128-ECB', $sKey, OPENSSL_RAW_DATA);
        return $decrypted;
    }
}
<?php
namespace app\common\lib\exception;
use think\Exception;

class ApiSuccessException extends Exception {
	public $code = 0;
	public $httpCode = 200;
	public $message = '';
	public $data = [];
	public $count = 0;

	public function __construct($message, $data=[], $code=0, $httpCode=200, $count=0) {
		$this->httpCode = $httpCode;
		$this->code = $code;
		$this->data = $data;
		$this->count = $count;
		$this->message = $message;
	}

}
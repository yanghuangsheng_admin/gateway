<?php
namespace app\common\lib\exception;
use think\Exception;

class ApiErrorException extends Exception {

	public $message = '';
	public $httpCode = 200;
	public $code = -1;
	public $data = [];

	public function __construct($message = '', $data=[], $code='-1', $httpCode=200) {
		$this->httpCode = $httpCode;
		$this->message = $message;
		$this->code = $code;
		$this->data = $data;
	}
}

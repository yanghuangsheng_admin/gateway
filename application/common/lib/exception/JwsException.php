<?php
namespace app\common\lib\exception;

use Exception;
use think\exception\Handle;
use think\exception\HttpException;
use think\exception\ValidateException;

class JwsException extends Handle
{
    protected $code = -1;
    protected $httpCode = 200;
    protected $data = [];
    protected $count = 0;

    public function render(Exception $e)
    {
        if(config('app_debug') == true) {
            return parent::render($e);
        }
        // 参数验证错误
        if ($e instanceof ApiSuccessException) {
            $this->httpCode = $e->httpCode;
            $this->code = $e->code;
            $this->data = $e->data;
            $this->count = $e->count;
        }

        if ($e instanceof ApiErrorException) {
            $this->httpCode = $e->httpCode;
            $this->code = $e->code;
            $this->data = $e->data;
        }

        $datas = [
            'code' => $this->code,
            'message' => $e->getMessage(),
            'data' => $this->data,
            'count' => $this->count,
        ];

        return json($datas, $this->httpCode);
    }

}
<?php
namespace app\admin\logic;

use app\common\model\User;
use think\Controller;
use app\common\lib\exception\ApiSuccessException;
use app\common\lib\exception\ApiErrorException;
use app\common\lib\IAes;
use think\facade\Session;

class Login extends Common
{
	public static function login() {
		if(parent::isPost()) {
			$data = input('param.');
			$userModel = new User();
			$model_data = $userModel->findUser($data['username']);
			if(empty($model_data)) {
				throw new ApiErrorException("没有该用户");
			}
			if(IAes::encryptAes($data['password']) != $model_data['password']) {
				throw new ApiErrorException("密码错误，清重新输入");
			}
			Session::set('id', $model_data['id']);
			throw new ApiSuccessException('登录成功');
		} else {
			throw new ApiErrorException('请求方式错误');
		}
	}
}
<?php
namespace app\admin\logic;

use think\Controller;
use app\common\lib\exception\ApiSuccessException;
use app\common\lib\exception\ApiErrorException;
use app\common\lib\IAes;
use think\facade\Session;

class User extends Common
{
	public static function find() {
		
	}

	public static function add() {
		if(parent::isPost()) {
			$data = input('param.');
			$userModel = new \app\common\model\User();
			$data['password'] = IAes::encryptAes($data['password']);
			$id = $userModel->addUser($data);

			if($id) {
				// redirect('/')->remember();
				throw new ApiSuccessException('注册成功');
			}
		} else {
			throw new ApiErrorException('请求方式错误');
		}
	}
}
<?php
namespace app\admin\logic;
use app\common\lib\exception\ApiErrorException;
use app\common\lib\exception\ApiSuccessException;
use think\Request;

class Common{
	/**
     * 是否是Post提交
     * @return bool
     */
    public static function isPost() {
        return \think\facade\Request::isPost();
    }

    /**
     * 是否是Get提交
     * @return bool
     */
    public static function isGet() {
        return \think\facade\Request::isGet();
    }

    /**
     * 是否是Delete提交
     * @return bool
     */
    public static function isDelete() {
        return \think\facade\Request::isDelete();
    }

    /**
     * 是否是Put提交
     * @return bool
     */
    public static function isPut() {
        return \think\facade\Request::isPut();
    }
}
<?php
namespace app\admin\controller;


use think\Controller;

class User extends Controller
{
	public function find() {
		if(\think\facade\Request::isGet()) {
			return self::fetch('admin@user/regist');
		} else {
			throw new ApiErrorException('请求方式错误');
		}
	}

	public function add() {
		\app\admin\logic\User::add();
	}
}
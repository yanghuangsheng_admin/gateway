<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件
use app\common\lib\exception\ApiErrorException;
use app\common\lib\exception\ApiSuccessException;
use app\common\lib\IAes;
use think\facade\Session;

/**
 * 检查是否登录
 * @return ApiErrorException
 */
function is_login() {
	$headers = \think\facade\Request::header();
	if (empty($headers['token'])){
        throw new ApiErrorException('token不存在',[],401,200);
    }
    $adminString = IAes::decryptAes($headers['token']);
    $admin = explode('||', $adminString);
    //如果缓存中不存在 抛出异常，如果缓存中存在,则设置username和type的值
    if (!Cache::get($admin[0])) {
        throw new ApiErrorException('您未登陆,请重新登陆',[],401,200);
    }
}

/**
 * 登录绑定token
 */
function setToken($username, $id) {
	$accessToken = IAes::encryptAes($username);
	//生成token
    \think\facade\Cache::set($id, $accessToken, 60*60*2);
    return $accessToken;
}

/**
 * 非前后端分离
 * @return true | false
 */
function session_login() {
	if(empty(Session::get('id'))) {
		return false;
	} else {
        return true;
    }
}

function getchatUser($user, $id) {
    return $user->getuser($id);
}
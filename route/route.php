<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

use think\facade\Route;
use think\facade\Session;
// Route::get('think', function () {
//     return 'hello,ThinkPHP5!';
// });


// Route::get('hello/:name', 'index/hello');

// return [

// ];

Route::group(['method' => 'get,post,put,delete'], function () {
	
	Route::get('/', 'index/Index/index'); #聊天接口
	Route::post('admin/login', 'admin/Login/login'); #提交登录
	Route::get('admin/regist', 'admin/User/find'); #查看注册页面
	Route::post('admin/registAdd', 'admin/User/add'); #保存注册内容

})->header('Access-Control-Allow-Origin','*')
    ->header('Access-Control-Allow-Credentials', 'true')
    ->header('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE')
    ->header('Access-Control-Allow-Headers','token,app-type,content-type,sign')
    ->allowCrossDomain();